#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h>
#include <unistd.h> // getcwd
#include <stdint.h>
#include <libgen.h>
#include <locale.h> // for strcoll
#include <stdbool.h>

//#define DBG

#ifdef DBG
#define dbg(...) printf("[dbg] \"%s\" %s:%i: ", __FILE__, __func__, __LINE__); printf(__VA_ARGS__);
#else
#define dbg(...)
#endif

// error

#define error(...) {fprintf(stderr, "\n\n>> ERROR in file \"%s\" in function \"%s\" in line %i: ", __FILE__, __func__, __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n\n"); exit(-1);}

// todo

#define todo(...) {printf("[todo] "); printf(__VA_ARGS__); printf("\n");}


#define olint uint64_t

typedef struct st_list t_list;

struct st_list{
	struct st_list *next;
	void *data;
};


typedef struct st_file_list t_file_list;
typedef struct st_dir_list t_dir_list;

struct st_file_list {
	const char *file_basename; // basename of this file
};

struct st_dir_list {
	const char *basename; // basename of dir
	const char *full_path; // full_path of dir
	t_list *file_list;
	t_list *sub_list;
	t_file_list **file_arr;
	t_dir_list **sub_arr;
	olint nb_files; // files in this dir (array size)
	olint nb_sub; // sub dirs in this dir (array size)
	bool link; // dir is link
};



void **listToArray(t_list **list, olint *array_size) {
	if (list == NULL) {
		error("list is null");
	}
 
	if (*list == NULL) {
		//error("*list is null");
		*array_size = 0;
		return NULL;
	}
 
	// count number of list elemets
	olint cnt = 0;
	t_list *p = *list;
	do {
		cnt++;
		p = p->next;
	} while (p != NULL);

	// allocate memory for array
	void **arr = malloc(cnt * sizeof(void *));
	if (arr == NULL) error("memory allocation failed for arr");

	// copy pointer of linked list to array
	p = *list;
	for (olint i=0; i<cnt; i++) {
		if (p == NULL) break; // this cannot happen?!
		arr[i] = p->data;
		p = p->next;
	}

	// free list (but keep data)
	p = *list;
	do {
		t_list *x = p;
		p = p->next;
		free(x);
	} while (p != NULL);
	*list = NULL;

	// return values
	*array_size = cnt;
	return arr;
}

void listFree(t_list *list){
	while (list != NULL)
	{
		t_list *l = list;
		list = list->next;
		if (l->data != NULL) free(l->data);
		free(l);
	}
}

void listFreeArray(void ***array, olint array_size) {
	if (array == NULL) return;
	if (*array == NULL) return;
	for (olint i=0; i<array_size; i++) {
		// array of pointer to data element
		// free data element first
		if ((*array)[i] != NULL) free((*array)[i]);
	}
	// free array itself
	free(*array);
	*array = NULL;
}

void dirListSubDirListToArray(t_dir_list *dir) {
	if (dir == NULL) {
		error("list is empty - dir pointing to null");
	}
	dir->sub_arr = (t_dir_list **) listToArray(&dir->sub_list, &dir->nb_sub);
}

bool dirExists(const char *dir) {
	struct stat sb;
	if (stat(dir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
		return true;
	} 
	return false;
}


extern char *getDirFromArgs(int argc, char* argv[]) {

	char * result = NULL;

	// argument provided?
	if ((argc>1) && (argv!=NULL)) {
		// allocate memory, copy string and truncate terminating '/'
		olint len = strlen(argv[1]);
		// reduce length in case of trailing '/'
		// +1 for \0
		result = malloc(sizeof(char)*(len+1));
		// copy str
		sprintf(result, "%s", argv[1]);
		// trailing '/' ?
		if (argv[1][len-1] == '/') 
			result[len-1] = '\0';
	} 
	// no argument: use current working dir as default
	else {
		char cwd[PATH_MAX+1];
		// get current working dir
		if (getcwd(cwd, sizeof(cwd)) == NULL) {
			perror("getcwd() error");
			error("cannot get current working directory");
			return NULL;
		}
		// +1 for \0
		result = malloc(sizeof(char)*(strlen(cwd)+1));
		sprintf(result, "%s", cwd);
	}
	// dir null? (bug)
	if (result == NULL) {
		error("result is null");
		return NULL;
	} 
	// dir exists?
	if (!dirExists(result)) {
		error("cannot find directory \"%s\"", argv[1]);
		return NULL;
	}

	return result;
}

t_dir_list *dirListNew(const char *basename, const char *full_path) {
	t_dir_list *dir = malloc(sizeof(t_dir_list));
	if (dir == NULL) error("memory allocation failed for dir");
	dir->basename = basename;
	dir->full_path = full_path;
	dir->file_list = NULL;
	dir->sub_list = NULL;
	dir->file_arr = NULL;
	dir->sub_arr = NULL;
	dir->nb_files = 0;
	dir->nb_sub = 0;
	dir->link = false;
	return dir;
}

void dirListFree(t_dir_list *dir) {
	if (dir == NULL) return;
	if (dir->basename != NULL) free((void *)dir->basename); // type cast because of const
	if (dir->full_path != NULL) free((void *)dir->full_path); // type cast because of const
	if (dir->sub_list != NULL) {
		todo("free list recursively");
		//		listFree(dir->sub_list);
		//		dir->sub_list = NULL;
	}
	if (dir->file_list != NULL) {
		// free recursively
		todo("free list recursively");
		//	       	listFree(dir->file_list);
		//		dir->file_list = NULL;
	}
	if (dir->sub_arr != NULL) {
		// free each element recursively
		for (olint i=0; i<dir->nb_sub; i++) {
			dirListFree(dir->sub_arr[i]);
		}
		// free array w/o elements
		free(dir->sub_arr);
		dir->sub_arr = NULL;
	}
	if (dir->file_arr != NULL) {
		listFreeArray((void ***)&dir->file_arr, dir->nb_files);
		dir->file_arr = NULL;
	}
	free(dir);
}


t_list *listNew(void *data)
{
	t_list *list = malloc(sizeof(t_list));
	if (list == NULL) error("memory allocation failed for list");
	list->next = NULL;
	list->data = data;
	return list;
}

void listAdd(t_list **list, void *data)
{
	if (list==NULL)
	{
		error("list pointer is null");
		exit(-1);
	}

	if (*list==NULL)
	{
		*list = listNew(data);
		return;
	}

	t_list *new = listNew(data); // new element
	new->next = *list;
	*list = new;
}

void dirListAddSubDir(t_dir_list *dir, const char *sub_basename, const char *sub_full_path) {
	// pointer checks
	if (dir == NULL) {
		error("list is empty - dir pointing to null");
	}

	// create dir list element (list data)
	t_dir_list *sub = malloc(sizeof(t_dir_list));
	if (sub == NULL) error("memory allocation failed for sub");
	sub->basename = sub_basename;
	sub->full_path = sub_full_path;
	sub->sub_arr = NULL;
	sub->file_arr = NULL;
	sub->sub_list = NULL;
	sub->file_list = NULL;
	sub->nb_files = 0;
	sub->nb_sub = 0;

	// add sub dir element as data in sub list of dir
	listAdd(&dir->sub_list, (void *) sub);
	// inc dir cnt
	dir->nb_sub++;
}


// ****************************************************************************************************
// tree
void show_tree(t_dir_list *dir, olint level, olint parents) {
	char *point = malloc(sizeof(char)*15);
	if (level>0) {
		if ((parents & (1 << level)) == 0)
			sprintf(point, "%s", "├── ");
		else
			sprintf(point, "%s", "└── ");
	} else {
		sprintf(point, "%s", "  ");
	}

	char indent[] = "   ";
	if (dir == NULL) error("dir is null");


	for (olint i=1; i<level; i++) {
		if ((parents & (1 << i)) == 0)
			printf("│");
		else
			printf(" ");
		printf("%s", indent);
	}

	if (level > 0) {
		if (dir->basename != NULL) 
			printf("%s%s\n", point, dir->basename);
		else if (dir->full_path != NULL)
			printf("%s%s\n", point, dir->full_path);
		else
			error("full path and basename are null");
	} else {
		printf("\n");
		if (dir->full_path != NULL)
			printf("%s%s\n", point, dir->full_path);
		else if (dir->basename != NULL) 
			printf("%s%s\n", point, dir->basename);
		else
			error("full path and basename are null");
		printf("│\n");
	}
	free(point);
	if (dir->sub_arr != NULL) {
		// for all subs
		for (olint i=0; i<dir->nb_sub; i++) {
			if ((level < 64) && (dir->sub_arr[i] != NULL))  {
				olint sub_parents = parents | ((i+1 == dir->nb_sub) << (level+1)); // set bit if last
				show_tree(dir->sub_arr[i], level+1, sub_parents);
			}
		}
		if (level == 0)
			printf("\n");
	} // else dir is empty - nothing to show 
}

void scan_tree(t_dir_list * dir) {
	if (dir == NULL) error("dir is null");

	DIR *os_dir = NULL;
	os_dir = opendir(dir->full_path);
	if (os_dir == NULL) return;
	struct dirent *child = NULL;
	while ((child = readdir(os_dir))) {
		struct stat child_stat;
		// copy child name
		char *child_name = NULL;
		child_name = malloc(sizeof(char)*(strlen(child->d_name)+1));
		sprintf(child_name, "%s", child->d_name);
		// copy child full path
		char *child_full_path = NULL;
		child_full_path = malloc(sizeof(char)*(strlen(dir->full_path)+strlen(child->d_name)+2));
		sprintf(child_full_path, "%s/%s", dir->full_path, child->d_name);
		// check if child is link (to dir or file)
		struct stat child_lstat;
		bool child_is_link = false;
		if ((lstat(child_full_path, &child_lstat) == 0) && ((child_lstat.st_mode & S_IFMT) == S_IFLNK)) {
			child_is_link = true;
		};
		if (stat(child_full_path, &child_stat) == 0) {
			if (strcmp(child->d_name, ".\0") == 0) {
				// dir is '.' --> ignore
				free(child_name);
				free(child_full_path);
			} else if (strcmp(child->d_name, "..\0") == 0) {
				// dir is '..' --> ignore
				free(child_name);
				free(child_full_path);
			} else if (child->d_name[0] == '.') {
				// file/dir is '.*' --> ignore hidden
				free(child_name);
				free(child_full_path);
			} else if ((child_stat.st_mode & S_IFMT) == S_IFDIR) {
				// create new sub dir in list
				dirListAddSubDir(dir, child_name, child_full_path);
				// update link property
				dir->link = child_is_link;
				// scan sub dir recursively (if no link)
				if (child_is_link == false)
					scan_tree((t_dir_list *)dir->sub_list->data);
			} else {
				// file is no dir --> ignore hidden
				free(child_name);
				free(child_full_path);
			}
		} else {
			free(child_name);
			free(child_full_path);
		}
	} 
	closedir(os_dir);

	// then dir list is done, now convert to array for easy access (even if empty, returns empty array)
	dirListSubDirListToArray(dir);
}

void upstr(char *s) {
	for (olint i = 0; s[i]!='\0'; i++) {
		if(s[i] >= 'a' && s[i] <= 'z') {
			s[i] = s[i] -32;
		}
	}
}


int subComparator(const void* p, const void* q) 
{ 
	t_dir_list *a = *(t_dir_list **) p;
	t_dir_list *b = *(t_dir_list **) q;
	//printf("qsort: %s %s\n", a->basename, b->basename);
	return strcoll(a->basename, b->basename);
} 

int setlocale_done = 0;
void sort_tree(t_dir_list *dir) {
	// setting locale (once) so that strcoll works
	if (setlocale_done == 0) {
		setlocale(LC_CTYPE, "");
		setlocale(LC_COLLATE, "");
		setlocale_done = 1;
	}
	if (dir == NULL) error ("dir is null");
	if (dir->nb_sub>0) {
		qsort((void*)(dir->sub_arr), dir->nb_sub, sizeof(void *), subComparator); 
		// sort all sub dirs recursively
		for (olint i=0; i<dir->nb_sub; i++) {
			if ((dir->sub_arr[i] != NULL) && (dir->sub_arr[i]->nb_sub>0))
				sort_tree(dir->sub_arr[i]);
		}
		// sort all sub dirs
	}
}

void tree(char *path) {

	// check root path name
	char *s_basename = NULL;
	if (path == NULL) {
		s_basename = malloc(sizeof(char)*(1+1));
		sprintf(s_basename, "%s", "'.'");
		path = malloc(sizeof(char));
		sprintf(path, "'%s'", "");
	} else {
		s_basename = malloc(sizeof(char)*(strlen(path)+1));
		sprintf(s_basename, "%s", basename(path));
	}

	// scan dirs and build tree
	t_dir_list *dir = dirListNew(s_basename, path);
	scan_tree(dir);

	// sort tree
	sort_tree(dir);

	// show tree
	show_tree(dir, 0, 0);

	// free memory
	dirListFree(dir);
}

int main(int argc, char* argv[]) {
	//system("clear");

	// get dir
	char *trunk_dir = getDirFromArgs(argc, argv); 

	// do it
	dbg("dir is \"%s\"\n", trunk_dir);
	tree(trunk_dir);

	// done
	return 0;
}
