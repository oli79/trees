
# tree (trees)

- **work in progress!**
- the project name should be ```tree```, but for some funny reason by ```gitlab```, this project name is a reserved name/word, so i am going with the plural, which is consistent with the other project names I have on ```gitlab```

![tree is a no-go](img/tree_name.png "tree is a no-go")


[[_TOC_]]


## screenshots and gifs

![treec test](img/tree_test.png "tree test")

## description

- partial recreation of ```tree -Cd``` [^1] in C
- no dependencies other than GNU libs
- for GNU/linux
- work in progress

[^1]: ```tree``` by Steve Baker (ice@mama.indstate.edu) https://github.com/nodakai/tree-command/tree/master (I am not sure if this is the official project site, sorry if I am wrong)


### todo
- colors
- special dirs (dead links, permission errors, etc.)
- cli (command line options)
- to be tested on 32bit and 64bit architecture
- to be tested on x86 and ARM (raspberry pi)
- natural sort (maybe)

## installation

- requires ```gcc``` and ```git```!

### release build

clone, make and run ```tree(s)```

```
git clone https://gitlab.com/oli79/trees
cd trees
make release
bin/tree <dir or glob>
```

### debug build

in order to compile with the compiler option ```-g``` omit the ```release``` key

```
git clone https://gitlab.com/oli79/trees
cd trees
make 
bin/tree <dir or glob>
```

## cli interface

- todo

## about me...

- i am looking for a position as a software developer in CH in the BE/SO area, available from august 2024
- i prefer to work on a GNU/linux host system (i.e. pc that runs linux)


## project email address
 
incoming+oli79-trees-55718216-issue-@incoming.gitlab.com


## footnotes

